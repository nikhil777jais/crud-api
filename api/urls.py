from django.contrib import admin
from django.urls import path
from django.urls.conf import include
from . import views
from rest_framework.routers import DefaultRouter

routers = DefaultRouter()
routers.register('userapi', views.MyUserView, basename='userapi')
urlpatterns = [
    # path('userinfo/', views.UserInfoView.as_view(), name='userinfo' ),
    # path('userinfo/<int:id>/', views.UserInfoView.as_view(), name='userinfo' ),
    path('', include(routers.urls)),
    path('auth/',include('rest_framework.urls')),
]