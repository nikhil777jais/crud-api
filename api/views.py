from django.shortcuts import render
from rest_framework import  viewsets
from .serializers import UserInfoSerializer
from rest_framework.response import Response
from .models import UserInfo
from rest_framework import status
from rest_framework.viewsets import ModelViewSet

from rest_framework.authentication import SessionAuthentication
from rest_framework.permissions import IsAdminUser, IsAuthenticated

#viewset based API
class MyUserView(viewsets.ModelViewSet):
  queryset = UserInfo.objects.all()
  serializer_class = UserInfoSerializer
  # authentication_classes = [BasicAuthentication]
  authentication_classes = [SessionAuthentication]
  permission_classes = [IsAuthenticated]

  def create(self, request): #eqivalen to post
    serializer = UserInfoSerializer(data=request.data)
    if serializer.is_valid():
      serializer.save()
      res = {"msg":"Created Data !!"}
      return Response(res, status = status.HTTP_201_CREATED)
    return Response(serializer.errors, status = status.HTTP_400_BAD_REQUEST)

  def update(self, request, pk=None):
    user = UserInfo.objects.get(pk=pk)
    serializer = UserInfoSerializer(user, data = request.data) #request.data get data from user instance
    print("User:",serializer)
    if serializer.is_valid():
      serializer.save()
      res = {"msg":"Completely Updated Data !!"}
      return Response(res, status = status.HTTP_201_CREATED)
    return Response(serializer.errors, status = status.HTTP_400_BAD_REQUEST) 

  def partial_update(self, request, pk=None):
    user = UserInfo.objects.get(pk=pk)
    serializer = UserInfoSerializer(user, data = request.data, partial = True) #request.data get data from user instance
    print("1")
    if serializer.is_valid():
      print("Validate ho raha hai")
      serializer.save()
      print("3")

      res = {"msg":"Patially Updated Data !!"}
      return Response(res, status = status.HTTP_201_CREATED)
    return Response(serializer.errors, status = status.HTTP_400_BAD_REQUEST) 
    
  def destroy(self, request, pk=None):  
    user = UserInfo.objects.get(pk=pk)
    user.delete()
    res = {"msg":"Deleted Data !!"}
    return Response(res, status = status.HTTP_200_OK)
