from django.core import validators
from .models import UserInfo
from rest_framework import serializers


class UserInfoSerializer(serializers.ModelSerializer):
  #validator For Phone Number:
  def check_phone(ph):
    for char in ph:
      if not char.isdigit():
        raise serializers.ValidationError("Enter Numbers only")
    if len(ph) != 10:
      raise serializers.ValidationError("Provide Correct Phone")
    return ph
    
  #validator For PassWord:
  def password_check(passwd):
    SpecialSym =['$', '@', '#', '%']
    val = True
    if len(passwd) < 6:
      val = False
      raise serializers.ValidationError("length should be at least 6")
    if len(passwd) > 12:
      val = False
      raise serializers.ValidationError('length should be not be greater than 12')
          
    if not any(char.isdigit() for char in passwd):
      val = False
      raise serializers.ValidationError('Password should have at least one numeral')
          
    if not any(char.isupper() for char in passwd):
      val = False
      raise serializers.ValidationError('Password should have at least one uppercase letter')
          
    if not any(char.islower() for char in passwd):
      val = False
      raise serializers.ValidationError('Password should have at least one lowercase letter')

    if not any(char in SpecialSym for char in passwd):
      val = False
      raise serializers.ValidationError('Password should have at least one of the symbols $@#')

    if val:
      return passwd

  phone = serializers.CharField(validators=[check_phone])
  password = serializers.CharField(validators=[password_check])
  class Meta:
    model = UserInfo
    fields = ['id', 'name', 'email', 'phone', 'password']
    


