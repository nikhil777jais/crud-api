from django.db import models
from django.contrib.auth.models import PermissionsMixin, AbstractBaseUser
from .managers import UserManager

# Create your models here.
class User(AbstractBaseUser, PermissionsMixin):
  email       = models.EmailField(max_length=250, unique=True,
                help_text='Email should be in prooper formate',)
  name        = models.CharField(max_length=250, blank=True, null=True)
  phone       = models.IntegerField(blank=True, null=True,)
  is_active   = models.BooleanField(default=True,
                help_text='Designates whether this user should be treated as active. '
                          'Unselect this instead of deleting accounts.')
  is_staff    = models.BooleanField(default=False,
                help_text='Designates whether the user can log into this admin site.')

  objects = UserManager()
  
  USERNAME_FIELD = 'email'
  REQUIRED_FIELDS = []

  # def natural_key(self):
  #   return self.email