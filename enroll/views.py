from django.contrib.messages.api import success
from django.http import request,  HttpResponseRedirect
from django.shortcuts import render
from django.contrib.auth.forms import AuthenticationForm, PasswordChangeForm
from django.contrib.auth import authenticate, login, logout, update_session_auth_hash
from . forms import EditAdminUserForm, Sign_up_form, EditUserForm
from django.contrib import messages
from django.contrib.auth import get_user_model
User = get_user_model()

# Create your views here.
#Singup view
def sign_up(request):
  if request.method == "POST":
    fm = Sign_up_form(request.POST)
    if fm.is_valid():
      fm.save()
      messages.success(request,'User Added Successfully!!')      
  else:
    fm = Sign_up_form()
  return render(request,'enroll/sign_up.html',{'form': fm})

#Login view
def log_in(request):
  if not request.user.is_authenticated:
    if request.method == 'POST':
      fm = AuthenticationForm(request=request, data=request.POST)
      if fm.is_valid():
        uemail = fm.cleaned_data['username']
        upass = fm.cleaned_data['password']
        user = authenticate(email=uemail, password=upass)
        if user is not None:
          login(request, user)
          messages.success(request,'Login Successfull!!')
          return HttpResponseRedirect('/profile/')  
    else:
      fm = AuthenticationForm()
    return render(request,'enroll/login.html',{'form': fm})
  else:  
    return HttpResponseRedirect('/profile/')
    
#logout view  
def log_out(request):
  logout(request)
  return HttpResponseRedirect('/login/') 

#User Profile
def profile(request):
  if request.user.is_authenticated:
    if request.method == 'POST':
      if request.user.is_superuser == True:
        fm = EditAdminUserForm(instance=request.user, data=request.POST)
        users = User.objects.all()
      else:
        fm = EditUserForm(instance=request.user, data=request.POST)
        users = None
      if fm.is_valid():
        fm.save()
        messages.success(request, 'Form Updated Successfully!!')
        return HttpResponseRedirect('/profile/')  
    else:
      if request.user.is_superuser == True:
        fm = EditAdminUserForm(instance=request.user)
        users = User.objects.all()
      else:  
        fm = EditUserForm(instance=request.user)
        users = None
    return render(request,'enroll/profile.html',{'name': request.user.email, 'form':fm, 'users': users },)
  else:
    return HttpResponseRedirect('/login/')  


#Password Change with old pass
def change_pass(request):
  if request.user.is_authenticated:
    if request.method == 'POST':
      fm = PasswordChangeForm(user=request.user, data=request.POST)
      if fm.is_valid():
        fm.save()
        update_session_auth_hash(request, fm.user)
        messages.success(request,'Password Updated Successfully')
        nm = request.user.email
        return HttpResponseRedirect('/profile/', {'name':nm })
    else:
      fm = PasswordChangeForm(user=request.user)
    return render(request,'enroll/changepass.html', {'form':fm} )
  else:
    return HttpResponseRedirect('/profile/')  

#details of user
def user_details(request,id):
  pi = User.objects.get(pk=id)
  fm = EditAdminUserForm(instance=pi)
  return render(request,'enroll/details.html', {'form':fm,'name':pi.email})
