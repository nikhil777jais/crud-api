from django.contrib.auth.forms import UserCreationForm,UserChangeForm
from django.contrib.auth import get_user_model
from django import forms
from django.forms import widgets
User = get_user_model()
class Sign_up_form(UserCreationForm):
  password2 = forms.CharField(label='Confirm Passwaord', widget=forms.PasswordInput)
  email = forms.EmailField(error_messages={'required':'Enter your Email'})
  class Meta:
    model = User
    fields =['email']
    labels = {'email':'Email'}
    widgets = {
            'email': forms.EmailInput(attrs={'placeholder':'user123@gmail.com'},),
    }
  
class EditUserForm(UserChangeForm):
  password= None
  class Meta:
    model = User
    fields = ['email','name', 'phone','last_login']
    
class EditAdminUserForm(UserChangeForm):
  password= None
  class Meta:
    model = User
    fields = '__all__'
    